import $ from "jquery";
import fancybox from "@fancyapps/fancybox";
window.fancybox = $.fancybox;

global.$ = global.jQuery = $;

let button = $('#btn')
button.on("click", function(e) {
    e.preventDefault()
    $.fancybox.open({
        src: '#popup__pay',
        type: 'inline'
    })
        localStorage.setItem('VisaName', $('#txt_name').val())
        localStorage.setItem('VisaMail', $('#email').val())
    var property = {
        name: $('#txt_name').val(),

        email: $('#email').val()
        }
});

$('#paypal-button-container').on('click', function(){

})
  
const smoothLinks = document.querySelectorAll('a[href^="#"]');
for (let smoothLink of smoothLinks) {
    smoothLink.addEventListener('click', function (e) {
        e.preventDefault();
        const id = smoothLink.getAttribute('href');

        document.querySelector(id).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
};
